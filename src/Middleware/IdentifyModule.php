<?php

namespace Hierarchy\Package\Middleware;

use Hierarchy\Package\Modules;
use Closure;

class IdentifyModule
{
    /**
     * @var Hierarchy\Package
     */
    protected $module;

    /**
     * Create a new IdentifyModule instance.
     *
     * @param Hierarchy\Package $module
     */
    public function __construct(Modules $module)
    {
        $this->module = $module;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $slug)
    {
        $request->session()->put('module', $this->module->getProperties($slug)->toArray());

        return $next($request);
    }
}
